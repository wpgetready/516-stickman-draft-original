﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class LevelFinish : MonoBehaviour {
	public	GameManager myGame;
	public Text bestScore;
	public Text currentscoretex;
	int currentscore;
	public GameObject star1;
	public GameObject star2;
	public GameObject star3;


	void Start(){
//		myGame = FindObjectOfType<GameManager> ();
		CheckHighScore ();
		currentscore = (int) myGame.currentscore;
	}

	void OnEnable ()
	{	
		Time.timeScale = 1;	
		PlayerPrefs.SetInt (SceneManager.GetActiveScene().name,1);
	}

	void CheckHighScore(){
		if ((int) myGame.currentscore > PlayerPrefs.GetInt (SceneManager.GetActiveScene ().name + "Score")) 
		{
			PlayerPrefs.SetInt (SceneManager.GetActiveScene ().name + "Score", (int) myGame.currentscore);
			Debug.Log (currentscore);
		}
	}
	void Update()
	{
		currentscoretex.text = Mathf.RoundToInt (currentscore).ToString();
	
		bestScore.text = PlayerPrefs.GetInt (SceneManager.GetActiveScene ().name + "Score").ToString();
		//Debug.Log (PlayerPrefs.GetInt (SceneManager.GetActiveScene ().name + "Score"));
		if(currentscore < 800){
			star3.SetActive (false);
		}
		if(currentscore < 600){
			star2.SetActive (false);
		}
		if(currentscore < 300){
			star1.SetActive (false);
		}
	}
	public void Fb(){
		Application.OpenURL ("https://www.facebook.com/The-King-Mobile-1901276070111235/");
	}

}
