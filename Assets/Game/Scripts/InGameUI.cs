﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;

//Note: for simplification, avoid adding states we won' use it!
public enum UIStates
{
    noState,gameCompleted,levelFailed,sceneRestart
}

public class InGameUI : MonoBehaviour {

    private UIStates myUIState = UIStates.noState;
    public static event System.Action<UIStates> UIStateChanged = delegate { };
    private const string URL_NEXT_APP = "https://play.google.com/store/apps/details?id=com.thekingmobile.stickmandraftblast";
    public UIStates UIState
    {
        get
        {
            return myUIState;
        }
        private set
        {
            if (value != myUIState)
            {
                Debug.Log("ZZZ: GameState: changing status from " + myUIState.ToString() + " to " + value.ToString());
                myUIState = value;
                Debug.Log("State changed to:" + myUIState.ToString());
                UIStateChanged(myUIState);
            }
        }
    }

    public GameObject All;
	public GameObject LevelFinish;  //The Level Finish Screen
	SoundController mySound;
	public GameObject pause; //The Pause Screen
	public bool GameComplete;

	void Start () {
		Time.timeScale = 1;
		All.SetActive (true);
		LevelFinish.SetActive (false);
		mySound = FindObjectOfType<SoundController> ();
	}
	void Update(){
	}
	void OnTriggerEnter2D(Collider2D other){
		if(other.tag.Contains("Over"))
		{
            UIState = UIStates.gameCompleted;
			GameComplete = true;
            if (mySound!=null)
            {
                mySound.fall.Play();
            } else
            {
                Debug.Log("SoundController missing. Are you testing this screen only?");

            }
            //Special case: check if we are in the last screen
            if (SceneManager.GetActiveScene().buildIndex ==51)
            {
                Application.OpenURL(URL_NEXT_APP); //we need to have a link to the next game!
                SceneManager.LoadScene("MainMenuUI");

            } else
            {
                LevelFinish.SetActive(true);
                All.SetActive(false);
                pause.SetActive(false);
            }

        }
	}
	public  void PauseButtonScript(){
        Time.timeScale = 0;
	}
	public void Home(){
		SceneManager.LoadScene ("MainMenuUI");
        if (mySound!=null)
        {
            mySound.button.Play();
        }
	}
	public void GameFinishScript(){
		Time.timeScale = 0;
	}
	public void LevelFailedScript(){
        UIState = UIStates.levelFailed;
		Time.timeScale = 0;
	}

	public void Restart(){
        UIState = UIStates.sceneRestart;
        SceneManager.LoadScene (SceneManager.GetActiveScene().name);
        if (mySound != null)
        {
            mySound.button.Play();
        }
		Time.timeScale= 1;
	}
	public void Resume(){
		Time.timeScale = 1;
	}
	public void NextLevel(){
        SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex + 1);
		Time.timeScale = 1;
		mySound.button.Play ();

	}
	public void PreviousLevel(){
        SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex - 1);
		Time.timeScale = 1;

	}
	public void Help(){
		Time.timeScale = 0;
	}
	public void Help_Back(){
		//Help_desk.SetActive (false);
		Time.timeScale = 1;
	}

	public void LoadLevels(){
        if  (mySound!=null)
        {
            mySound.button.Play();
        }
		PlayerPrefs.SetInt ("main", 1);
		SceneManager.LoadScene ("MainMenuUI");
	}
}
