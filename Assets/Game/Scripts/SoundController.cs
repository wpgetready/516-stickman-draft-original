﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundController : MonoBehaviour {

	public AudioSource fall;
	public AudioSource gameBg;
	public  AudioSource tap;
	public AudioSource button;
	public static SoundController instance;
	public GameObject volumeOn;
	public GameObject volum;

	void Start () {
		PlayerPrefs.SetInt ("sound", 1);
		if (!instance) {
			DontDestroyOnLoad (this.gameObject);
			instance = this;
		} else {
			DestroyImmediate (this.gameObject);

		}
	}
	
	// Update is called once per frame
	void Update () {
		if (PlayerPrefs.GetInt ("sound") == 1) {
			fall.volume = 1;
			gameBg.volume = 1;
			tap.volume = 1;
			button.volume = 1;
			
		}
		if(PlayerPrefs.GetInt ("sound") == 0){
			fall.volume = 0;
			gameBg.volume = 0;
			tap.volume = 0;
			button.volume = 0;
		}
		
	}
	public void Button(){
		button.Play ();
	}

	public void  VolumeOn(){
		PlayerPrefs.SetInt ("sound", 1);
		volumeOn.SetActive (true);
		volum.SetActive (false);


	}
	public void volumeOff(){
		PlayerPrefs.SetInt ("sound", 0);
		volumeOn.SetActive (false);
		volum.SetActive (true);

	}
}
