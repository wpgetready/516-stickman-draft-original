﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public enum LevelStates
{
    noState, loadLevel,loadMainMenu,sceneRestart,scenePaused,sceneResumed,sceneNext,scenePrevious
}

public class LevelManager : MonoBehaviour {

    private LevelStates myLevelState = LevelStates.noState;
    public static event System.Action<LevelStates> LevelStateChanged= delegate { };


    public LevelStates levelState
    {
        get
        {
            return myLevelState;
        }
        private set
        {
            if (value != myLevelState)
            {
                myLevelState = value;
                LevelStateChanged(myLevelState);
            }
        }
    }

    LockedLevels levelIndicator;

	public string levelName;
    
	int newNum;
	void Start () {
		Time.timeScale = 1;
		levelIndicator = FindObjectOfType<LockedLevels> ();

	}
	void Update () {
		
	}

    public  void LoadLevel(){
		string name =EventSystem.current.currentSelectedGameObject.name;
		SceneManager.LoadScene (name);
        levelState = LevelStates.loadLevel;


    }
	public void MianMenuLoad(){
		
		SceneManager.LoadScene ("MainMenuUI");
        levelState = LevelStates.loadMainMenu;
    }
	public void Restart(){
		SceneManager.LoadScene (SceneManager.GetActiveScene().name);
		Time.timeScale= 1;
        levelState = LevelStates.sceneRestart;
    }
	public void Paused(){
		Time.timeScale = 0;
        levelState = LevelStates.scenePaused;
    }
	public void Resume(){
		Time.timeScale = 1;
        levelState = LevelStates.sceneResumed;
    }
	public void NextLevel(){
		SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex + 1);
		Time.timeScale = 1;

        levelState = LevelStates.sceneNext;
    }
	public void PreviousLevel(){
		SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex - 1);
		Time.timeScale = 1;
        levelState = LevelStates.scenePrevious;

    }
}
