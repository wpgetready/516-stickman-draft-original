﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

//This class control a list of images for creating a image sequence.

public class FadeImageList : MonoBehaviour {

    [Serializable]
    public class ImageStructure
    {
        public Image img;
        public float fadeIn =1f;
        public float showTime= 3f;
        public float fadeOut = 1f;
    }

    public ImageStructure[] imageList;
    public GameObject EnableScriptHideRope; //Enable script allowing to click and end the screen
    public GameObject ChangeColor;          //Object that we need to change color.

    void Awake()
    {
        //hideImages(); //Hide every image when starting
    }

    // Use this for initialization
    void Start () {
        hideImages();
        StartCoroutine( displayImages());
		
	}

    private void hideImages()
    {
        foreach (ImageStructure item in imageList)
        {
            Color tmp = item.img.color;
            tmp.a = 0;
            item.img.color = tmp;
        }
    }
    /*
    //Esto anda, aunque no queda claro porque pasan ciertas cosas
    private IEnumerator displayImages()
    {
        foreach (ImageStructure item in imageList)
        {
            Debug.Log("Calling fadein..." + item.fadeIn);
            //fade in
            StartCoroutine( item.fadeIn.Tweeng((u) => { Color tmp = item.img.color; tmp.a = u; item.img.color = tmp; }, 0f, 1f));
            yield return new WaitForSeconds(item.fadeIn);
            Debug.Log("Calling showTime..." + item.showTime);
            //wait n seconds, where I do NOTHING
            StartCoroutine(item.showTime.Tweeng((u) => { Color tmp = item.img.color; tmp.a = u; item.img.color = tmp; }, 1f, 1));
            yield return new WaitForSeconds(item.showTime+item.fadeIn);
            //fade out
            Debug.Log("Calling fadeout..." + item.fadeOut);
            StartCoroutine(item.fadeOut.Tweeng((u) => { Color tmp = item.img.color; tmp.a = u; item.img.color = tmp; }, 1f, 0f));
            yield return new WaitForSeconds(item.fadeOut+item.fadeIn+item.showTime);
        }
        
    }
    */

    private IEnumerator displayImages()
    {
        foreach (ImageStructure item in imageList)
        {
            Debug.Log("Calling fadein..." + item.fadeIn);
            //fade in
            StartCoroutine(item.fadeIn.Tweeng((u) => { Color tmp = item.img.color; tmp.a = u; item.img.color = tmp; }, 0f, 1f));
            yield return new WaitForSeconds(item.fadeIn);
            Debug.Log("Calling showTime..." + item.showTime);
            //wait n seconds, where I do NOTHING
            StartCoroutine(item.showTime.Tweeng((u) => { Color tmp = item.img.color; tmp.a = u; item.img.color = tmp; }, 1f, 1));
            yield return new WaitForSeconds(item.showTime + item.fadeIn);
            //fade out
            Debug.Log("Calling fadeout..." + item.fadeOut);
            //If fade out is 0, just leave the image and don't hide it.
            if (! (item.fadeOut ==0)) { 
            StartCoroutine(item.fadeOut.Tweeng((u) => { Color tmp = item.img.color; tmp.a = u; item.img.color = tmp; }, 1f, 0f));
            yield return new WaitForSeconds(item.fadeOut + item.fadeIn + item.showTime);
            }
        }
        Debug.Log("Iteration ended.");
        //Enable script for clicking
        if (EnableScriptHideRope !=null)
        {
            EnableScriptHideRope.GetComponent<Hide_Rope>().enabled = true;
        }
        else
        {
            Debug.Log("ERROR: EnableScriptHideRope not assigned!");
        }
        if (ChangeColor !=null)
        {
            ChangeColor.GetComponent<SpriteRenderer>().color = new Color(255f, 255f, 255f);
        } else
        {
            Debug.Log("ERROR: ChangeColor not assigned!");
        }
        
        


    }
}
