﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;


//This is an example of fading using Extension Methods.
//Please read links from that class to understand this example.

public class FadeImage : MonoBehaviour
{

    //Attach an Image you want to fade in the GameObject's Inspector
    public Image m_Image;
    //Use this to tell if the toggle returns true or false
    public  bool m_Fading;


    float test;

    void Start()
    {
        // fade in alpha in .75 seconds
        StartCoroutine(2f.Tweeng((u) => { Color tmp = m_Image.color; tmp.a = u; m_Image.color = tmp; }, 0f, 1f));
    }
}